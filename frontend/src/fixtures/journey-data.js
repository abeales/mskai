[
  {
    "id": "mr-op",
    "clone": null,
    "title": "Base operation",
    "invitable": false,
    "keyValues": {},
    "children": [
      {
        "id": "mr-tkr",
        "clone": "mr-op",
        "title": "Total Knee Arthroplasty",
        "invitable": true,
        "keyValues": { "joint": "knee" },
        "children": [
          {
            "id": "client-a-tkr",
            "clone": "mr-tkr",
            "title": "Client A: Total Knee Arthroplasty",
            "invitable": true,
            "keyValues": { "client": "client-a", "joint": "knee" },
            "children": []
          },
          {
            "id": "client-b-tkr",
            "clone": "mr-tkr",
            "title": "Client B: Total Knee Arthroplasty",
            "invitable": true,
            "keyValues": { "client": "client-b", "joint": "knee" },
            "children": [
              {
                "id": "client-b-pkr",
                "clone": "client-b-tkr",
                "title": "Client B: Partial Knee Arthroplasty",
                "invitable": true,
                "keyValues": {
                  "client": "client-b",
                  "joint": "knee",
                  "partial": true
                },
                "children": []
              }
            ]
          }
        ]
      },
      {
        "id": "mr-thr",
        "clone": "mr-op",
        "title": "Total Hip Arthroplasty",
        "invitable": true,
        "keyValues": { "joint": "hip" },
        "children": [
          {
            "id": "client-a-thr",
            "clone": "mr-thr",
            "title": "Client A: Total Hip Arthroplasty",
            "invitable": true,
            "keyValues": { "client": "client-a", "joint": "hip" },
            "children": []
          },
          {
            "id": "client-b-thr",
            "clone": "mr-thr",
            "title": "Client B: Total Hip Arthroplasty",
            "invitable": true,
            "keyValues": { "client": "client-b", "joint": "hip" },
            "children": []
          }
        ]
      }
    ]
  }
]
