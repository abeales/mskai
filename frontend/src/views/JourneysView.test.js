import { render, screen, waitFor } from '@testing-library/vue'
import JourneysView from './JourneysView.vue'
import 'regenerator-runtime/runtime'


const heading = "Journey Tree"

describe('Initial display', () => {

  test('Initial view renders', async () => {
    render(<JourneysView />);
    await waitFor(() => {
      expect(screen.queryByText(heading)).toBeInTheDocument()
    })
  })
})
