import { createStore } from 'vuex'
import JourneysService from '../services/JourneysService'

export default createStore({
  state: {
    journeys: []
  },
  mutations: {
    SET_JOURNEYS (state, data) {
      state.journeys = data
    }
  },
  actions: {
    fetchJourneys ({ commit }) {
      return JourneysService.get()
        .then(resp => {
          commit('SET_JOURNEYS', resp.data)
        })
    }
  }
})