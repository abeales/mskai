import json
import os

from starlette.testclient import TestClient


parent_dir = os.path.dirname(__file__)
fixtures_dir = os.path.join(parent_dir, "json-examples")

with open(os.path.join(fixtures_dir, "full-journey-tree.json")) as f1:
    JOURNEY_TREE = json.load(f1)

with open(os.path.join(fixtures_dir, "full-journey-tree.json")) as f2:
    JOURNEY_LIST = json.load(f2)


def test_get_journeys(client: TestClient):

    resp = client.get("/get-journeys")

    resp_json = resp.json()

    assert resp_json == JOURNEY_TREE, (
        f"{json.dumps(resp_json, indent=2)}"
        "\n DOES NOT EQUAL \n"
        f"{json.dumps(JOURNEY_TREE, indent=2)}"
    )
