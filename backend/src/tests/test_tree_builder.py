import json
import os

from journeys.tree_builder import JourneyTreeBuilder

parent_dir = os.path.dirname(__file__)
json_fixtures_dir = os.path.join(parent_dir, "json-examples")

with open(os.path.join(json_fixtures_dir, "flat-journey-list.json")) as f1:
    JOURNEY_LIST = json.load(f1)

with open(os.path.join(json_fixtures_dir, "full-journey-tree.json")) as f2:
    JOURNEY_TREE = json.load(f2)

with open(os.path.join(json_fixtures_dir, "smaller-journey-tree.json")) as f3:
    SMALLER_JOURNEY_TREE = json.load(f3)

yaml_fixtures_dir = os.path.join(parent_dir, "test-yaml")


def test_tree_builder_can_build_journey_tree():
    """
    Test that JourneyTreeBuilder class can build tree correctly,
    as regular Python dict and Pydantic object
    """
    tree_builder = JourneyTreeBuilder()
    tree = tree_builder.build_tree()
    tree["clone"] = None

    assert [tree] == JOURNEY_TREE, (
        f"{json.dumps(tree, indent=2)}"
        "\n DOES NOT EQUAL \n"
        f"{json.dumps(JOURNEY_TREE, indent=2)}"
    )


def test_tree_builder_can_build_journey_tree_from_alternative_yaml_files():
    """
    Use alternative YAML directory and files to check that tree builder can
    build trees correctly from other source files, not just the original YAML
    """
    tree_builder = JourneyTreeBuilder(directory=yaml_fixtures_dir)
    tree = tree_builder.build_tree()
    tree["clone"] = None

    assert [tree] == SMALLER_JOURNEY_TREE, (
        f"{json.dumps(tree, indent=2)}"
        "\n DOES NOT EQUAL \n"
        f"{json.dumps(JOURNEY_TREE, indent=2)}"
    )


def test_tree_builder_can_build_flat_list():
    tree_builder = JourneyTreeBuilder()
    assert tree_builder._create_journey_list() == JOURNEY_LIST
