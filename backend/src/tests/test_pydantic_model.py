from journeys.schemas import JourneyTree


JOURNEY_NODES_EXAMPLE = {
    "id": "mr-thr",
    "clone": "mr-op",
    "title": "Total Hip Arthroplasty",
    "invitable": True,
    "keyValues": {"joint": "hip"},
    "children": [
        {
            "id": "client-a-thr",
            "clone": "mr-thr",
            "title": "Client A: Total Hip Arthroplasty",
            "invitable": True,
            "keyValues": {
                "client": "client-a",
                "joint": "hip"
            },
            "children": []
        },
        {
            "id": "client-b-thr",
            "clone": "mr-thr",
            "title": "Client B: Total Hip Arthroplasty",
            "invitable": True,
            "keyValues": {
                "client": "client-b",
                "joint": "hip"
            },
            "children": []
        }
    ]
}


def test_pydantic_model():
    JourneyTree(**JOURNEY_NODES_EXAMPLE)
