import pytest
from starlette.testclient import TestClient

# from api.config import Settings, get_settings
from api.main import app


# def get_settings_override():
#     return Settings(testing=1)


@pytest.fixture(name="client", scope="module")
def test_app():
    # app.dependency_overrides[get_settings] = get_settings_override

    print("Test Client setting up.......")

    with TestClient(app) as test_client:
        yield test_client
