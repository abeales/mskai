from typing import List

from fastapi import APIRouter

from journeys.tree_builder import JourneyTreeBuilder
from journeys.schemas import JourneyTree

router = APIRouter()


@router.get("/get-journeys", response_model=List[JourneyTree])
async def get_journeys():

    tree_builder = JourneyTreeBuilder()
    tree = tree_builder.build_pydantic_tree()

    # AmCharts expects root node to be inside a list
    return [tree]
