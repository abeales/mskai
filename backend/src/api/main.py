from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from api.routers import get_journeys


def create_application() -> FastAPI:
    application = FastAPI()

    application.include_router(get_journeys.router)

    return application


app = create_application()


origins = [
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
