from typing import List

from pydantic import BaseModel


class JourneyTree(BaseModel):
    id: str
    clone: str | None
    title: str
    invitable: bool
    keyValues: object  # could make this separate schema
    children: List["JourneyTree"]
