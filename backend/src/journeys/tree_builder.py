import glob
import json
import os
from typing import List

from ruamel.yaml import YAML

from journeys.schemas import JourneyTree


# default directory for YAML journeys
parent_dir = os.path.dirname(__file__)
yaml_dir = os.path.join(parent_dir, "journey-yaml")


class JourneyTreeBuilder:

    def __init__(self, directory: str = yaml_dir):
        """initialize the class with the directory location for YAML files"""
        self.yaml_dir = directory

    def _parse_yaml_journey(self, filepath) -> dict:
        """Read YAML journey file and convert to Python dict"""
        with open(filepath, 'r') as f:
            yaml = YAML()
            journey = yaml.load(f.read())
        return journey

    def _create_journey_list(self) -> List:
        """
        - load and parse each YAML journey within the given directory as a dict
        - add an ID for the journey node, obtained from the filename
        - return a list of journey dicts
        """
        filepaths = glob.glob(f"{self.yaml_dir}/**/*.yaml", recursive=True)
        return [
            {
                "id": path.split("/")[-1].replace("-journey.yaml", ""),
                **self._parse_yaml_journey(path),
            } for path in filepaths
        ]

    def _get_children(self, parent_id: str, relations: List) -> List:
        """The recursive function for building a node tree"""
        children = [r for r in relations if r.get("clone") == parent_id]
        return [
            {
                **c,
                "children": self._get_children(c["id"], relations)
            } for c in children
        ]

    def build_tree(self) -> dict:
        """Construct full tree of journey nodes (nested dicts)"""
        journeys = self._create_journey_list()
        [root_node] = [j for j in journeys if "clone" not in j]
        return {
            **root_node,
            "children": self._get_children("mr-op", journeys)
        }

    def build_pydantic_tree(self) -> JourneyTree:
        """Alternative option: construct tree of typed Pydantic objects"""
        tree = self.build_tree()
        return JourneyTree(**tree)


if __name__ == "__main__":
    tree_builder = JourneyTreeBuilder(directory=yaml_dir)
    tree = tree_builder.build_tree()
    tree_pydantic = tree_builder.build_pydantic_tree()

    # journey tree as dict
    print(json.dumps(tree, indent=2))

    # journey tree as Pydantic object
    print(tree_pydantic)
