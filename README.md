# MSK.ai Assigned Task

Backend and frontend code are in the same repo for simplicity (would not normally do this).

```bash
git clone https://gitlab.com/abeales/msk
```



## Backend API (FastAPI)



### Installation

The installation instructions below use virtualenv to set up the FastAPI app. It requires version 3.10 and up (for the ruamel.yaml package). The Python version can be managed using a version manager such as Pyenv (https://github.com/pyenv/pyenv) or within a Docker container. Instructions below use Pyenv.

```bash
# clone the repo
$ git clone https://gitlab.com/abeales/msk

# Python 3.10 and up required (this way if using Pyenv)
$ cd msk
$ pyenv local 3.10.2

# create virtual environment and install requirements
$ cd backend/src
$ python -m venv env
$ source env/bin/activate
$ pip install -r requirements.txt
```

### Usage

```bash
# run the FastAPI app
$ uvicorn api.main:app --port 8000 --host 0.0.0.0 --reload
```

Make sure the backend app is running when running the frontend

Auto-generated docs available at http://localhost:8000/docs

The app will serve the tree hierarchy of Journeys.

Try adding or removing Journey YAML files in the directory:`/backend/src/journeys/journey-yaml/`, and the API will return the updated tree hierarchy.

### Tests

```bash
# run the tests
$ python -m pytest
```





## Frontend (Vue)



### Install and run

```bash
# in a separate terminal window
$ cd/frontend
$ npm install
$ npm run serve
```

Then navigate to http://localhost:8080

Press the button to call the API and then "Show Tree". Hover over each node to display its properties.

